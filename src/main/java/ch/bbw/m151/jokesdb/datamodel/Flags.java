package ch.bbw.m151.jokesdb.datamodel;

import lombok.Data;

@Data
public class Flags {
    boolean nsfw;
    boolean religious;
    boolean political;
    boolean racist;
    boolean sexist;
    boolean explicit;
}
