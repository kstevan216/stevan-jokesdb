package ch.bbw.m151.jokesdb.datamodel;

import lombok.Data;

@Data
public class JokesDto {
    boolean error;
    String category;
    String type;
    String joke;
    Flags flags;
    boolean safe;
    int id;
    String lang;
}