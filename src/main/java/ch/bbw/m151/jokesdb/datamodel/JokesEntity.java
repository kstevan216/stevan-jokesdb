package ch.bbw.m151.jokesdb.datamodel;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;

@Data
@Accessors(chain = true)
@Entity
@Table(name = "jokes")
public class JokesEntity {

	@Id
	@GeneratedValue
	int id;

	@Column(nullable = false)
	String joke;

}
