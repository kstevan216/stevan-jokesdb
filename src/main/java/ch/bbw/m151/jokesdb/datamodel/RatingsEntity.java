package ch.bbw.m151.jokesdb.datamodel;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class RatingsEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private double rating;

    @OneToOne
    @JoinColumn
    private JokesEntity joke;

    @Transient
    private int joke_id;

}
