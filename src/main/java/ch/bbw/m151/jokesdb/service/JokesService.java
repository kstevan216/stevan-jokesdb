package ch.bbw.m151.jokesdb.service;

import ch.bbw.m151.jokesdb.datamodel.JokesDto;
import ch.bbw.m151.jokesdb.repository.JokesRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

@Service
public class JokesService {

    private static final Logger log = LoggerFactory.getLogger(JokesService.class);

    private final JokesRepository jokesRepository;

    public JokesService(JokesRepository jokesRepository) {
        this.jokesRepository = jokesRepository;
    }

    /*@EventListener(ContextRefreshedEvent.class)
    public void preloadDatabase() {
        if (jokesRepository.count() != 0) {
            log.info("database already contains data...");
            return;
        }
        log.info("will load jokes from classpath...");
        try (var lineStream = Files.lines(new ClassPathResource("chucknorris.txt").getFile()
                .toPath(), StandardCharsets.UTF_8)) {
            var jokes = lineStream.filter(x -> !x.isEmpty())
                    .map(x -> new JokesEntity().setJoke(x))
                    .toList();
            jokesRepository.saveAll(jokes);
        } catch (IOException e) {
            throw new RuntimeException("failed reading jokes from classpath", e);
        }
    }*/

    public boolean doesJokeExist(int id) {
        return jokesRepository.existsById(id);
    }

    public JokesDto getJoke() {
        var client = WebClient.builder()
                .baseUrl("https://v2.jokeapi.dev/joke/")
                .build();
        return client.get()
                .uri("Any?lang=de&type=single")
                .retrieve()
                .bodyToMono(JokesDto.class)
                .block();

    }
}
