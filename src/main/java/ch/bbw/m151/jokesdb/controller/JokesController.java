package ch.bbw.m151.jokesdb.controller;

import ch.bbw.m151.jokesdb.datamodel.JokesDto;
import ch.bbw.m151.jokesdb.datamodel.JokesEntity;
import ch.bbw.m151.jokesdb.repository.JokesRepository;
import ch.bbw.m151.jokesdb.service.JokesService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.List;

@Slf4j
@RestController
public class JokesController {

    private final JokesRepository jokesRepository;
    private WebClient client = WebClient.create("https://jokeapi.dev/");
    @Autowired
    private JokesService service;

    public JokesController(JokesRepository jokesRepository) {
        this.jokesRepository = jokesRepository;
    }

    @GetMapping("/joke")
    public JokesEntity getJoke() {
        log.info("Getting Joke");

        JokesDto jokesDto = service.getJoke();
        JokesEntity jokesEntity = new JokesEntity();
        jokesEntity.setId(jokesDto.getId());
        jokesEntity.setJoke(jokesDto.getJoke());

        if (!service.doesJokeExist(jokesEntity.getId())) {
            jokesRepository.save(jokesEntity);
        }

        return jokesEntity;

    }

    /**
     * @param pageable to be called with params `?page=3&size=5`
     * @return hilarious content
     */
    @GetMapping("/jokes")
    public List<JokesEntity> getJokes(Pageable pageable) {
        return jokesRepository.findAll(pageable)
                .getContent();
    }
}
