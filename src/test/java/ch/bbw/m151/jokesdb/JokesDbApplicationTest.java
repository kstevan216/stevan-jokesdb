package ch.bbw.m151.jokesdb;

import ch.bbw.m151.jokesdb.datamodel.JokesEntity;
import ch.bbw.m151.jokesdb.repository.JokesRepository;
import ch.bbw.m151.jokesdb.service.JokesService;
import org.assertj.core.api.WithAssertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.reactive.server.WebTestClient;

import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
@AutoConfigureMockMvc
public class JokesDbApplicationTest implements WithAssertions {

    @Autowired
    JokesRepository jokesRepository;

    @Autowired
    JokesService jokesService;

    @Autowired
    private WebTestClient webTestClient;

    @Test
    void jokesAreLoadedAtStartup() {
        var jokes = jokesRepository.findAll();
        assertThat(jokes).hasSizeGreaterThan(100)
                .allSatisfy(x -> assertThat(x.getJoke()).isNotEmpty());
    }

    @Test
    void jokesCanBeRetrievedViaHttpGet() {
        var pageSize = 5;
        webTestClient.get()
                .uri("/jokes?page={page}&size={size}", 1, pageSize)
                .exchange()
                .expectStatus()
                .is2xxSuccessful()
                .expectBodyList(JokesEntity.class)
                .hasSize(pageSize);
    }

    @Test
    void jokesAreUnique() {
        JokesEntity joke = new JokesEntity();
        joke.setId(1);
        joke.setJoke("This is my funny joke hahahaha");

        assertTrue(jokesService.doesJokeExist(1));
    }
}
